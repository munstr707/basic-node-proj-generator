#!/usr/bin/env node
const inquirer = require("inquirer");
const colors = require("colors");
const path = require("path");

const buildProjectFromAnswers = require("./src/projectBuilder.js");

const QUESTIONS = [
  {
    name: "project-name",
    type: "input",
    message: "Project name: ",
    validate: (input) =>
      /^([A-Za-z\-\_\d])+$/.test(input) ||
      "Project name may only include letters, numbers, underscores and hashes."
        .red,
  },
];

inquirer
  .prompt(QUESTIONS)
  .then(
    buildProjectFromAnswers({
      templateProjectPath: path.join(__dirname, "template", "npm-project"),
    })
  );
