module.exports = name => {
  const speak = phrase => console.log(`${name} woofs ${phrase}`);

  return Object.freeze({
    speak
  });
};
