# Basic Node Project Generator

## Purpose

Generic project generator wrapper that you can easily stub projects into _basic_node_proj_generator_ to create your own generator.

## Description

Currently _basic_node_proj_generator_ checks for a project within the `template` directory whenever the user prompts the command for this project specified in `"bin"` of `package.json`. After being provided with a project name, the generator takes the project from the `template` directory and copies all the file and directory contents of that project into a new project of the specified name. Upon completing this all the node modules are installed for the project.

## How To Use ( Developer )

1.  Replace `template/npm-project` with your project under `template`
2.  Within `index.js` specify the project name from `1.`
3.  In `package.json` under `"bin"` change `"build-sample-node-proj"` to whatever name you want the generator to be invoked by.
4.  All done! Test and publish to wherever you host your repositories and _npm_

## How To Use ( User Experience )

The user invokes the specified command from above for the custom project generator, and then they are prompted with a question asking for the name of their desired project. After entering a valid name the project is generated with a basic loading animation.
